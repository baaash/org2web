;;; org2web.el --- Use Org Mode to publish your website -*- lexical-binding: t -*-
;;
;; Author: Cooper Oscarfono <bleat@baaa.sh>
;; URL: https://gitlab.com/baaash/org2web.git
;; Version: 1.0
;; Package-Version: 20230330.0001
;; Package-Commit:
;; Package-Requires:
;; Keywords: org, website, publishing
;; License: Public Domain
;;
;; This file is not part of GNU Emacs.
;;
;; This is free and unencumbered software released into the public domain.
;;
;; Anyone is free to copy, modify, publish, use, compile, sell, or distribute
;; this software, either in source code form or as a compiled binary, for any
;; purpose, commercial or non-commercial, and by any means.
;;
;; In jurisdictions that recognize copyright laws, the author or authors of
;; this software dedicate any and all copyright interest in the software to
;; the public domain. We make this dedication for the benefit of the public
;; at large and to the detriment of our heirs and successors. We intend this
;; dedication to be an overt act of relinquishment in perpetuity of all present
;; and future rights to this software under copyright law.
;;
;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
;; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
;; AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
;; ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
;; WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
;;
  ;;; Commentary:
;;
;; This package provides a simple way to use Org Mode to publish a website.  It
;; includes functionality to generate HTML, handle image and video assets, and
;; start a local web server to serve the published site.
;;
  ;;; Code:
(require 'package)
(setq package-user-dir (expand-file-name "./.packages"))
(setq package-archives '(("melpa" . "https://melpa.org/packages/")
                         ("elpa" . "https://elpa.gnu.org/packages/")))

;; List of required packages
(defvar required-packages '(use-package))

;; Install required packages if they are not already installed
(defun ensure-packages ()
  (dolist (package required-packages)
    (unless (package-installed-p package)
      (package-install package))))

;; Initialize package management and install required packages
(package-initialize)
(ensure-packages)

(use-package org
  :ensure t
  :config
  (require 'ox-publish)

  (defun get-org-component-contents (component)
    "Retrieve the HTML contents of the specified org component."
    (with-temp-buffer
      (insert-file-contents (concat "./src/components/" component ".org"))
      (org-export-to-buffer 'html "*Org HTML Export*" nil nil nil t)
      (buffer-string)))

  (setq header-contents (get-org-component-contents "header"))
  (setq footer-contents (get-org-component-contents "footer"))

  (setq org-publish-project-alist
        `(("content"
           :base-directory "./src/content/"
           :base-extension "org"
           :publishing-directory "./public/"
           :recursive t
           :publishing-function org-html-publish-to-html
           :headline-levels 4
           :auto-preamble nil
           :auto-sitemap t
           :sitemap-title "Sitemap"
           :sitemap-sort-files anti-chronologically
           :sitemap-file-entry-format "%d %t"
           :with-author nil
           :with-creator t
           :with-date nil
           :with-timestamps nil
           :with-toc nil
           :section-numbers nil
           :html-doctype "html5"
           :html-html5-fancy t
           :html-head-include-scripts nil
           :html-head-include-default-style nil
           :html-head  "<link rel=\"stylesheet\" href=\"/styles/global.css\" />
<link rel=\"icon\" href=\"/static/favicon.ico\" type=\"image/x-icon\" />"
           :html-preamble ,header-contents
           :html-postamble ,footer-contents
           :html-divs ((preamble "header" "preamble")
                        (content "main" "content")
                        (postamble "footer" "postamble"))
           :html-validation-link nil
           :makeindex t)
          ("assets"
           :base-directory "./src/assets"
           :base-extension any
           :publishing-directory "./public"
           :recursive t
           :publishing-function org-publish-attachment
           :html-divs nil)

          ("baaa.sh" :components ("content" "assets")))))


;; Publish our site and notify us when it's ready.
(org-publish-all t)

(message "org files published successfully")

(use-package simple-httpd
  :ensure t
  :config
  (setq httpd-root "./public"
        httpd-port 8080
        httpd-host "localhost")
  (defun my-httpd-start ()
    "Start HTTP server for `httpd-root' on `httpd-port'."
    (interactive)
    (when (get-buffer "*httpd*")
      (kill-buffer "*httpd*"))
    (message "Starting web server...")

    (httpd-start)

    (message "HTTP server started on http://%s:%d" httpd-host httpd-port))
  (defun my-httpd-stop ()
    "Stop the HTTP server."
    (interactive)
    (httpd-stop)
    (message "HTTP server stopped")))

(my-httpd-start)

(read-event)

(provide 'org2web)
    ;;; org2web.el ends here
