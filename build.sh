#!/bin/bash

set -euo pipefail

# Check if inotify-tools is installed
if ! command -v inotifywait &> /dev/null; then
    echo "inotify-tools is required, please install it"
    exit 1
fi

# Check if port 8080 is available
if lsof -Pi :8080 -sTCP:LISTEN -t >/dev/null ; then
    echo "Port 8080 is already in use"
    exit 1
fi

# Remove contents of public folder
echo "Removing contents of Public Directory to regenerate"
rm -rf ./public/*

# Web Server control functions
start_web_server() {
    # Check if the web server is already running
    if [[ -v EMACS_PID ]]; then
        echo "Stopping previous web server with PID: $EMACS_PID"
        kill "$EMACS_PID"
    fi
    # Start the web server
    emacs -Q --script org2web.el &
    EMACS_PID=$!
    echo "Web server started with PID: $EMACS_PID"
}

stop_web_server() {
    if [[ -v EMACS_PID ]]; then
        echo "Stopping web server with PID: $EMACS_PID"
        kill "$EMACS_PID"
    fi
}

# Set trap to stop the web server if the script is interrupted
trap stop_web_server INT

# Start the web server
start_web_server

# Monitor changes in the source directory and its subdirectories (excluding hidden files and directories, sitemap.org, and files with .inc extension) and regenerate the website
inotifywait -m -r -e create --exclude '(/\.|/node_modules|/sitemap.org|\.inc$)' ./src | while read -r directory events filename; do
    if [ "$filename" == "" ]; then
        continue
    fi
    echo "Regenerating website due to $events in $filename"
    start_web_server
done
